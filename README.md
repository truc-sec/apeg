APEG
====

### Another Pirate Exploration Game.  

Votre but est de vous déplacer sur la map à l'aide des touches "AZEQDWXC", jusqu'à trouver les trésors (les petites boîtes marron).  
Une fois que vous avez la pelle, vous devez creuser sur chaque case afin de trouver le trésor enterré sur l'île.  
Les autres objets (mousquet et armure) vous permettent respectivement de survivre à 50% par une attaque de pirate (et d'attaquer les pirates à distance), et et survivre à 90% à une attaque de pirate (si vous avez les deux objets alors vous survivez à 100%).  
Les "B" sont des Boucanniers. Ils peuvent se déplacer de une ou deux cases à chaque tour, et vous attaquent quand vous vous trouvez sur la même case que lui.  
Les "F" sont les Flibustiers, qui eux ne se déplacent que d'une case, mais qui vous attaque si vous êtes une case à côté d'eux.  

Votre but au début de la partie est donc d'esquiver les pirates pour chercher les objets dans les coffres visibles.  


### Screenshots

![third+step](http://l3m.in/p/up/files/1513855443.png "FINI")  
Version actuelle



![second+step+two](http://l3m.in/p/up/files/1513791215.png "C'est limite jouable.")  
Il ya peu de temps  



![second+step](http://l3m.in/p/up/files/1513787841.png "Okay tout va bien.")  
Il ya peu de temps  



![first+step](http://l3m.in/p/up/files/1513245695.png "Bon ben ça compile.")  
Il y a très longtemps