main = main$(EXE_EXT)

BINS-CXX-y := $(main)

objs := \
    main.cpp.o \
    class/Game.cpp.o \
    class/Unite.cpp.o \
    class/Objet.cpp.o \
    class/Joueur.cpp.o \
    class/Ennemi.cpp.o \
    generalFunctions.cpp.o \
    
OBJS-$(main)-y := $(objs)