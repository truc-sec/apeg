#ifndef JOUEUR_H
#define JOUEUR_H
#include "Unite.h"

	class Joueur : public Unite{
		std::string name;
		bool objets[3];

		public:
			Joueur();
			Joueur(std::string);
			bool addObject(std::string);
			bool haveObject(std::string);
			std::string getInfos();
			bool deplacement(char);
	};

#endif
