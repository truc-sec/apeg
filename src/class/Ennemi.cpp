#include <iostream>
#include <unistd.h>
#include "Ennemi.h"
#include "../generalFunctions.h"

using namespace std;

	Ennemi::Ennemi(){
		if(randomNumber(1,50) > 25)
			type = true;
		else type = false;
		if(type)
			cout << "Création d'un boucannier." << endl;
		else
			cout << "Création d'un flibustier." << endl;
	}

	/// true = boucannier, false = flibustier
	bool Ennemi::getInfos(){
		return type;
	}

	void Ennemi::deplacement(){
		while(!move(randomNumber(1,8)));
	}