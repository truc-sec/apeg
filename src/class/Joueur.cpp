#include <iostream>
#include <unistd.h>
#include "Objet.h"
#include "Joueur.h"

using namespace std;


	Joueur::Joueur(){
		name = "Default";
		objets[0] = false;
		objets[1] = false;
		objets[2] = false;
	}

	Joueur::Joueur(string s){
		cout << "Création de " << s << "." << endl;
		name = s;
	}

	bool Joueur::addObject(string s){
		if(s == "pelle" && !objets[0]){
			objets[0] = true;
			return true;
		}
		if(s == "mousquet" && !objets[1]){
			objets[1] = true;
			return true;
		}
		if(s == "armure" && !objets[2]){
			objets[2] = true;
			return true;
		}
		return false;
	}

	bool Joueur::haveObject(string s){
		if(s == "pelle" && objets[0])
			return true;
		if(s == "mousquet" && objets[1])
			return true;
		if(s == "armure" && objets[2])
			return true;
		return false;
	}

	string Joueur::getInfos(){
		return name;
	}

	bool Joueur::deplacement(char action){
		switch (action){
			case 'a':
				if(move(5)){
					cout << "déplacement en haut à gauche" << endl;
					return true;
				}
				else{
					cout << "vous ne pouvez pas vous déplacer en haut à gauche" << endl;
					return false;
				}
				break;
			case 'z':
				if(move(1)){
					cout << "déplacement en haut" << endl;
					return true;
				}
				else{
					cout << "vous ne pouvez pas vous déplacer en haut" << endl;
					return false;
				}
				break;
			case 'e':
				if(move(7)){
					cout << "déplacement en haut à droite" << endl;
					return true;
				}
				else{
					cout << "vous ne pouvez pas vous déplacer à droite" << endl;
					return false;
				}
				break;
			case 'q':
				if(move(3)){
					cout << "déplacement à gauche" << endl;
					return true;
				}
				else{
					cout << "vous ne pouvez pas vous déplacer à gauche" << endl;
					return false;
				}
				break;
			case 'd':
				if(move(4)){
					cout << "déplacement à droite" << endl;
					return true;
				}
				else{
					cout << "vous ne pouvez pas vous déplacer à droite" << endl;
					return false;
				}
				break;
			case 'w':
				if(move(6)){
					cout << "déplacement en bas à gauche" << endl;
					return true;
				}
				else{
					cout << "vous ne pouvez pas vous déplacer en bas à gauche" << endl;
					return false;
				}
				break;
			case 'x':
				if(move(2)){
					cout << "déplacement en bas" << endl;
					return true;
				}
				else{
					cout << "vous ne pouvez pas vous déplacer en bas" << endl;
					return false;
				}
				break;
			case 'c':
				if(move(8)){
					cout << "déplacement en bas à droite" << endl;
					return true;
				}
				else{
					cout << "vous ne pouvez pas vous déplacer à droite" << endl;
					return false;
				}
				break;
			case '<':
				cout << "Aide: ZQDX = haut/gauche/droite/bas, AEWC = diagonales, S = creuser, R = attaquer" << endl;
				return false;
				break;
			default:
				cout << "aucune action n'est associée à cette touche ("<< action <<")" << endl;
				return false;
				break;
		}
		return false;
	}