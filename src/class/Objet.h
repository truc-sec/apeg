#ifndef OBJET_H
#define OBJET_H
#include "Unite.h"

	class Objet : public Unite{
		std::string name;

		public:
			Objet();
			Objet(std::string);
			bool remove();
			std::string getName();
	};

#endif