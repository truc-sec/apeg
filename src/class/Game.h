#ifndef GAME_H
#define GAME_H
#include "Unite.h"
#include "Objet.h"
#include "Unite.h"
#include "Joueur.h"
#include "Ennemi.h"
//#include "Input.h"
//#include "Output.h"

#include <vector>

	class Game{
		int nbJoueurs;
		int nbEnnemis;
		std::vector<Joueur> joueurs;
		std::vector<Ennemi> ennemis;
		std::vector<Objet> objets;
		int foundTresor;
		Objet tresor;
		public:
			Game();
			bool play();
			void victory();
			void defeat();
			bool atLeastOnePlayerIsAlive();
			void joueurPlay(int);
			bool deplacement(char, int);
			bool creuser(char, int);
			bool combattre(char, int);
			void ennemiPlay(int);
			void checkEvents();
			bool combat(bool, bool);
			bool checkPositionEnnemi(Ennemi, Joueur);
			bool checkPositionObjet(Objet, Joueur);
			void draw();
			void greetings();
			void writeTextLetterByLetter(std::string, int);
			void clrScr();
			char getch();
	};

#endif
