#include <iostream>
#include <unistd.h>
#include "Objet.h"
#include "../generalFunctions.h"

#include "Unite.h"

using namespace std;

	Unite::Unite(){
		alive = true;
		x = randomNumber(0,11);
		y = randomNumber(0,11);
	}

	bool Unite::isAlive(){
		return alive;
	}

	bool Unite::move(int direction){
		switch(direction){
			case 1:
				if(x>0){
					x--;
					return true;
				}
				break;
			case 2:
				if(x<11){
					x++;
					return true;
				}
				break;
			case 3:
				if(y>0){
					y--;
					return true;
				}
				break;
			case 4:
				if(y<11){
					y++;
					return true;
				}
				break;
			case 5:
				if(x>0 && y>0){
					x--;
					y--;
					return true;
				}
				break;
			case 6:
				if(x<11 && y>0){
					x++;
					y--;
					return true;
				}
				break;
			case 7:
				if(x>0 && y<11){
					x--;
					y++;
					return true;
				}
				break;
			case 8:
				if(x<11 && y<11){
					x++;
					y++;
					return true;
				}
				break;
			default:
				break;
		}
		return false;
	}

	int Unite::getX(){
		return x;
	}

	int Unite::getY(){
		return y;
	}

	bool Unite::kill(){
		if(alive){
			alive = false;
			return true;
		}
		return false;
	}