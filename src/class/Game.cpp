#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <ctime>
#include <fstream>
#include <termios.h>

#include "Game.h"
#include "Unite.h"
#include "Objet.h"
#include "Unite.h"
#include "Joueur.h"
#include "Ennemi.h"
#include "../generalFunctions.h"
//#include "Input.h"
//#include "Output.h"

using namespace std;

	Game::Game(){

		greetings();

		cout << "Combien de joueurs ?\n>";
		cin >> nbJoueurs;

		writeTextLetterByLetter(">>Début du jeu. Initialisation...\n>>Spawn des pirate.\n", 15000);
		
		nbEnnemis = 3+(nbJoueurs-1);
		ennemis.resize(nbEnnemis);
		writeTextLetterByLetter(">>Spawn des pirates terminé.\n>>Spawn des joueurs.\n", 15000);

		joueurs.resize(nbJoueurs);

		for(int i(0); i<nbJoueurs; i++){
			Joueur p("Jacques");
			joueurs[i] = p;
		}
		writeTextLetterByLetter(">>Spawn des joueurs terminé.\nSpawn des objets.\n", 15000);

		objets.resize(3*nbJoueurs);
		for(int i(0); i<3*nbJoueurs; i+=3){
			Objet o1("pelle");
			objets[i] = o1;
			Objet o2("mousquet");
			objets[i+1] = o2;
			Objet o3("armure");
			objets[i+2] = o3;
		}
		foundTresor = -1;

		writeTextLetterByLetter(">>Spawn des objets terminé.\n>>Initialisation terminée.\n\n", 15000);

		writeTextLetterByLetter("Good luck!", 25000);
	}


	bool Game::play(){

		while(atLeastOnePlayerIsAlive() && foundTresor==-1){

			// PLAYER TURN
			for(int i(0); i<nbJoueurs; i++){
				draw();
				joueurPlay(i);
				checkEvents();
				usleep(800000);
			}
			// AI TURN
			for(int i(0); i<nbEnnemis; i++){
				draw();
				ennemiPlay(i);
				checkEvents();
				usleep(50000);
			}
		}
		return false;
	}

	void Game::joueurPlay(int i){
		string pelle, mousquet, armure;
		if(joueurs[i].haveObject("pelle"))
			pelle = "pelle";
		else
			pelle = "\e[90mpelle\e[39m";

		if(joueurs[i].haveObject("mousquet"))
			mousquet = "mousquet";
		else
			mousquet = "\e[90mmousquet\e[39m";

		if(joueurs[i].haveObject("armure"))
			armure = "armure";
		else
			armure = "\e[90marmure\e[39m";


		cout << "Au tour de " << joueurs[i].getInfos() << " ("<< i <<").\n["<< pelle <<"]["<< mousquet <<"]["<< armure <<"]" << endl;
		char a;
		
		fflush(stdout);
		a = getch();
		while(!deplacement(a, i) && !creuser(a, i) && !combattre(a, i))
			a = getch();
	}

	bool Game::deplacement(char a, int i){
		return (a == 'a' || a == 'z' || a == 'e' || a == 'q' || a == 'd' || a == 'w' || a == 'x' || a == 'c' || a == '<') && joueurs[i].deplacement(a);
	}

	bool Game::creuser(char a, int i){
		if(a != 's')
			return false;

		if(!joueurs[i].haveObject("pelle")){
			cout << "Vous n'avez pas de pelle, vous ne pouvez pas creuser." << endl;
			return false;
		}
		if(tresor.getX() == joueurs[i].getX() && tresor.getY() == joueurs[i].getY()){
			foundTresor = i;
			cout << "Vous sentez quelque chose de dur sous votre pelle." << endl;
			return true;
		}
		cout << "Vous ne trouvez rien." << endl;
		return false;
	}

	bool Game::combattre(char a, int i){
		if(a != 'r')
			return false;

		if(!joueurs[i].haveObject("mousquet")){
			cout << "Vous n'avez pas de mousquet, vous ne pouvez pas attaquer à distance." << endl;
			return false;
		}

		for(int j(0); j<nbEnnemis; j++){
			if(ennemis[j].getX() >= joueurs[i].getX()-1 && ennemis[j].getX() <= joueurs[i].getX()+1 && ennemis[j].getY() >= joueurs[i].getY()-1 && ennemis[j].getY() <= joueurs[i].getY()+1){
				if(ennemis[j].getInfos())
					cout << "Vous attaquez un boucannier." << endl;
				else
					cout << "Vous attaquez un flibustier." << endl;

				if(joueurs[i].haveObject("mousquet") && joueurs[i].haveObject("armure")){
					ennemis[j].kill();
					cout << "Vous tuez l'ennemi !" << endl;
					return true;
				}
				if(joueurs[i].haveObject("mousquet")){
					if(randomNumber(1,100)>50){
						ennemis[j].kill();
						if(ennemis[j].getInfos())
							cout << "Vous tuez un Boucannier !" << endl;
						else
							cout << "Vous tuez un Flibustier !" << endl;
						return true;
					}
					else{
						joueurs[i].kill();
						cout << "Dommage, l'ennemi est plus fort que vous. Vous êtes mort." << endl;
						return true;
					}
				}
			}
		}
		cout << "Il n'y a pas d'ennemi proche de vous, vous ne pouvez pas attaquer." << endl;
		return false;
	}

	void Game::ennemiPlay(int i){
		// AI TURN
		if(ennemis[i].isAlive()){
			if(ennemis[i].getInfos() && randomNumber(1,50) > 25){
				ennemis[i].deplacement();
				draw();
				usleep(350000);
				ennemis[i].deplacement();	
			}
			else
				ennemis[i].deplacement();
			usleep(350000);
		}
	}

	void Game::draw(){
		clrScr();

		bool emptyCell;
		for(int i(0); i<12; i++){
			for(int j(0); j<12; j++){
				emptyCell = true;
				bool joueur = false;
				int nbJoueur = 0;
				bool ennemi = false;
				bool typeEnnemi = false;
				bool objet = false;
				for(int k(0); k<nbJoueurs; k++){
					if(joueurs[k].getX() == i && joueurs[k].getY() == j){
						emptyCell = false;
						joueur = true;
						nbJoueur = k;
						//cout << "\e[38;5;75m "<< k <<"\e[39m";
					}
				}
				for(int k(0); k<nbEnnemis; k++){
					if(ennemis[k].getX() == i && ennemis[k].getY() == j && ennemis[k].isAlive()){
						emptyCell = false;
						ennemi = true;
						//cout << "\e[33m";
						typeEnnemi = ennemis[k].getInfos();
						//cout << "\e[39m";
					}
				}
				for(int k(0); k<nbJoueurs*3; k+=3){
					for(int l(0); l<3; l++){
						if(objets[k+l].getX() == i && objets[k+l].getY() == j && objets[k+l].isAlive()){
							emptyCell = false;
							objet = true;
							//cout << "\e[38;5;95m[]\e[39m";
						}
					}
				}
				cout << "\e[40;38;5;251m";
				if(emptyCell)
					cout << " ×";
				else{
					if(joueur && ennemi && objet)
						cout << "\e[38;5;75mX\e[33mX\e[39m";
					if(joueur && ennemi && !objet){
						if(typeEnnemi)
							cout << "\e[38;5;44m" << nbJoueur << "\e[33mB\e[39m";
						else
							cout << "\e[38;5;44m" << nbJoueur << "\e[33mF\e[39m";
					}
					if(joueur && !ennemi && objet)
						cout << "\e[38;5;44m" << nbJoueur << "\e[38;5;130m▤\e[39m";
					if(!joueur && ennemi && objet){
						if(typeEnnemi)
							cout << "\e[33mB\e[38;5;130m▤\e[39m";
						else
							cout << "\e[33mF\e[38;5;130m▤\e[39m";
					}
					if(joueur && !ennemi && !objet)
						cout << " \e[38;5;44m" << nbJoueur << "\e[39m";
					if(!joueur && ennemi && !objet){
						if(typeEnnemi)
							cout << " \e[33mB\e[39m";
						else
							cout << " \e[33mF\e[39m";
					}
					if(!joueur && !ennemi && objet)
						cout << " \e[38;5;130m▤\e[39m";
				}
				cout << "\e[49m";
			}
			cout << endl;
		}
	}

	bool Game::atLeastOnePlayerIsAlive(){
		for(int i(0); i<nbJoueurs; i++){
			if(joueurs[i].isAlive()) return true;
		}
		return false;
	}


	void Game::checkEvents(){

		// combat automatique contre un ennemi
		for(int i(0); i<nbEnnemis; i++){
			for(int j(0); j<nbJoueurs; j++){
				if(ennemis[i].isAlive() && checkPositionEnnemi(ennemis[i], joueurs[j])){
					if(!combat(joueurs[j].haveObject("mousquet"), joueurs[j].haveObject("armure"))){
						cout << joueurs[j].getInfos() << " ("<< j <<") s'est fait tuer." << endl;
						usleep(1000000);
						joueurs[j].kill();
					}
					else
						if(joueurs[j].haveObject("mousquet")){
							cout << "Un ennemi s'est fait tuer par " << joueurs[j].getInfos() << " (" << j << ") !" << endl;
							usleep(1000000);
							ennemis[i].kill();
						}
				}
			}
		}

		// trouver un objet
		for(int i(0); i<nbJoueurs*3; i++){
			for(int j(0); j<nbJoueurs; j++){
				if(objets[i].isAlive() && checkPositionObjet(objets[i], joueurs[j]) && !joueurs[j].haveObject(objets[i].getName())){
					joueurs[j].addObject(objets[i].getName());
					objets[i].kill();
					cout << joueurs[j].getInfos() << " ("<< j <<") a trouvé [" << objets[i].getName() << "] !" << endl;
					usleep(1000000);
				}
				else if(objets[i].isAlive() && checkPositionObjet(objets[i], joueurs[j]) && joueurs[j].haveObject(objets[i].getName())){
					cout << joueurs[j].getInfos() << " ("<< j <<") a déjà l'objet [" << objets[i].getName() << "]. Le ramasser ne sert à rien." << endl;
					usleep(1000000);
				}
			}
		}
	}

	bool Game::checkPositionEnnemi(Ennemi e, Joueur j){
		if(e.getInfos())
			return e.getX() == j.getX() && e.getY() == j.getY();
		else
			return e.getX() >= j.getX()-1 && e.getX() <= j.getX()+1 && e.getY() >= j.getY()-1 && e.getY() <= j.getY()+1;
	}

	bool Game::checkPositionObjet(Objet o, Joueur j){
		return j.getX() == o.getX() && j.getY() == o.getY();
	}

	bool Game::combat(bool mousquet, bool armure){
		if(mousquet && armure)
			return true;
		if(mousquet)
			return randomNumber(1,100)>50;
		if(armure)
			return randomNumber(1,100)>90;
		return false;

	}

	// PAS IMPORTANT

	void Game::greetings(){
		clrScr();
		writeTextLetterByLetter("Bienvenue dans APEG.", 100000);
		cout << endl;
		writeTextLetterByLetter("(another pirate exploration game)\n\n(pour afficher l'aide en jeu, appuyez sur la touche \"<\")", 30000);
		usleep(400000);
		cout << endl;
	}

	void Game::writeTextLetterByLetter(string s, int delay){
		for(unsigned int i(0); i<s.length(); i++){
			cout << s[i];
			fflush(stdout); // thanks http://stackoverflow.com/a/2259329
			usleep(delay);
		}
	}

	/// Clear the screen. Thanks http://stackoverflow.com/q/4062045
	void Game::clrScr(){
			cout << "\033[2J\033[1;1H";
	}

		/// Getter-from-buffer function. Thanks http://stackoverflow.com/a/912796
	char Game::getch(){
		char buf = 0;
		struct termios old = {0};
		if(tcgetattr(0, &old) < 0)
			perror("tcsetattr()");
		old.c_lflag &= ~ICANON;
		old.c_lflag &= ~ECHO;
		old.c_cc[VMIN] = 1;
		old.c_cc[VTIME] = 0;
		if(tcsetattr(0, TCSANOW, &old) < 0)
			perror("tcsetattr ICANON");
		if(read(0, &buf, 1) < 0)
			perror ("read()");
		old.c_lflag |= ICANON;
		old.c_lflag |= ECHO;
		if(tcsetattr(0, TCSADRAIN, &old) < 0)
			perror ("tcsetattr ~ICANON");
		return (buf);
	}