#ifndef ENNEMI_H
#define ENNEMI_H
#include "Unite.h"

	class Ennemi : public Unite{
		bool type;

		public:
			Ennemi();
			bool getInfos();
			void deplacement();
	};

#endif
