#include <iostream>
#include <unistd.h>

#include "Objet.h"

using namespace std;

	Objet::Objet(){
		name = "Tresor";
		kill();
	}

	Objet::Objet(string n){
		if(n == "mousquet")
			cout << "Création d'un mousquet." << endl;
		else
			cout << "Création d'une " << n << "." << endl;
		name = n;
	}

	bool Objet::remove(){
		if(this->kill())
			return true;
		return false;
	}

	string Objet::getName(){
		return name;
	}