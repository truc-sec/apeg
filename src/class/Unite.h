#ifndef UNITE_H
#define UNITE_H

	class Unite{
		bool alive;
		int x;
		int y;

		public:
			Unite();
			bool isAlive();
			bool move(int);
			int getX();
			int getY();
			bool kill();
	};

#endif
